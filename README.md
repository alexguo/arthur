# arthur
springboot构建的spring cloud微服务项目框架V1.0
项目详细介绍见 https://my.oschina.net/u/2893418/blog/1512091

### 使用方法
数据库、缓存服务器等配置文件在以下项目，arthur项目直接读取git上的config-center项目
https://git.oschina.net/ArthurFamily/config-center.git
也可以在arthur项目中的
arthur-config-center中配置spring:cloud:config配置为本地文件模式
依次启动注册中心、配置中心、turbine、网关等项目，其中arthur-manage-serviceWeb与arthur-manage-registrationProcess模块为子项目模块，
开发时可仿照二者之间的交互进行各模块（各服务）同步开发。

### TODO
接下来的版本将集成进前端框架，形成一整套CMS框架。


